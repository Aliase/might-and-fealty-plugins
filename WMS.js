/**
 * @author Teh Hsao Chai (Aliase) For Might and Fealty Route modify control, extend from OpenLayers.Control.ModifyFeature control. Added function to
 *         delete vertex with a click Added function to define if the start point and or end point can be modified
 */

OpenLayers.Layer.WMSMAF = OpenLayers.Class(OpenLayers.Layer.WMS, {

	features_vector: null,

	isMerged: false,

	zoomChanged: false,

	lastZoom: 0,

	isZoomOut: false,

	realmsVectors: new Array(),

	layerOrder: {
		realms: 0,
		empires: 1,
		kingdoms: 2,
		duchies: 3,
		marches: 4,
		counties: 5,
		baronies: 6
	},

	labelLayer: null,

	/**
	 * WMS realm layer level 0->realms 1->empires 2->kingdoms 3->duchies 4->marches 5->counties 6->baronies
	 */
	layerLevel: 0,

	/**
	 * Constructor: OpenLayers.Layer.WMS Create a new WMS layer object Examples: The code below creates a simple WMS layer using the image/jpeg
	 * format. (code) var wms = new OpenLayers.Layer.WMS("NASA Global Mosaic", "http://wms.jpl.nasa.gov/wms.cgi", {layers: "modis,global_mosaic"});
	 * (end) Note the 3rd argument (params). Properties added to this object will be added to the WMS GetMap requests used for this layer's tiles. The
	 * only mandatory parameter is "layers". Other common WMS params include "transparent", "styles" and "format". Note that the "srs" param will
	 * always be ignored. Instead, it will be derived from the baseLayer's or map's projection. The code below creates a transparent WMS layer with
	 * additional options. (code) var wms = new OpenLayers.Layer.WMS("NASA Global Mosaic", "http://wms.jpl.nasa.gov/wms.cgi", { layers:
	 * "modis,global_mosaic", transparent: true }, { opacity: 0.5, singleTile: true }); (end) Note that by default, a WMS layer is configured as
	 * baseLayer. Setting the "transparent" param to true will apply some magic (see <noMagic>). The default image format changes from image/jpeg to
	 * image/png, and the layer is not configured as baseLayer. Parameters: name - {String} A name for the layer url - {String} Base url for the WMS
	 * (e.g. http://wms.jpl.nasa.gov/wms.cgi) params - {Object} An object with key/value pairs representing the GetMap query string parameters and
	 * parameter values. options - {Object} Hashtable of extra options to tag onto the layer. These options include all properties listed above, plus
	 * the ones inherited from superclasses.
	 */
	initialize: function(name, url, params, options)
	{
		var newArguments = [];
		// uppercase params
		params = OpenLayers.Util.upperCaseObject(params);
		if (parseFloat(params.VERSION) >= 1.3 && !params.EXCEPTIONS)
		{
			params.EXCEPTIONS = "INIMAGE";
		}
		newArguments.push(name, url, params, options);
		OpenLayers.Layer.Grid.prototype.initialize.apply(this, newArguments);
		OpenLayers.Util.applyDefaults(this.params, OpenLayers.Util.upperCaseObject(this.DEFAULT_PARAMS));

		// layer is transparent
		if (!this.noMagic && this.params.TRANSPARENT && this.params.TRANSPARENT.toString().toLowerCase() == "true")
		{

			// unless explicitly set in options, make layer an overlay
			if ((options == null) || (!options.isBaseLayer))
			{
				this.isBaseLayer = false;
			}

			// jpegs can never be transparent, so intelligently switch the
			// format, depending on the browser's capabilities
			if (this.params.FORMAT == "image/jpeg")
			{
				this.params.FORMAT = OpenLayers.Util.alphaHack() ? "image/gif" : "image/png";
			}
		}
		this.labelLayer = options.labelLayer;
		this.events.on({
			"loadend": this.onLoadEnd,
			scope: this
		})
	},

	/**
	 * Method: getURL Return a GetMap query string for this layer Parameters: bounds - {<OpenLayers.Bounds>} A bounds representing the bbox for the
	 * request. Returns: {String} A string with the layer's url and parameters and also the passed-in bounds and appropriate tile size specified as
	 * parameters.
	 */
	getURL: function(bounds)
	{
		bounds = this.adjustBounds(bounds);

		var imageSize = this.getImageSize();
		var newParams = {};
		// WMS 1.3 introduced axis order
		var reverseAxisOrder = this.reverseAxisOrder();
		newParams.BBOX = this.encodeBBOX ? bounds.toBBOX(null, reverseAxisOrder) : bounds.toArray(reverseAxisOrder);
		newParams.WIDTH = imageSize.w;
		newParams.HEIGHT = imageSize.h;
		var requestString = this.getFullRequestString(newParams);
		return requestString;
	},

	setEvent: function()
	{
		this.map.events.on({
			"moveend": this.onMoveEnd,
			"movestart": this.onMoveStart,
			scope: this
		});

		this.lastZoom = this.map.getZoom();
	},

	setLayerLevel: function(level)
	{
		this.layerLevel = this.layerOrder[level];
		this.mergeNewParams({
			"layers": [this.realmsVectors[this.layerLevel].layer]
		});
	},

	onMoveEnd: function(evt)
	{

		if (typeof (this.realmsVectors) != "undefined" && this.realmsVectors != null)
		{

			if (typeof (this.zoomChanged) != "undefined")
			{
				if (this.zoomChanged == true)
				{
					// zoom in
					if (this.map.getZoom() > this.lastZoom)
					{
						// If zoom in, do nothing to realm levels
						log("zoom in");
					}
					// zoom out
					else
					{
						// if zoom out, check if the realm level above have 2 or more feature that is contained if view port.
						// if there is 2 or above, display realm level ++
						// if there is 1, display current realm level
						// if there is 0, go to next realm level
						log("zoom out");
						if (this.layerLevel != 0)
						{
							var features = this.realmsVectors[--this.layerLevel].features;
							var go = this.goPreviousLevel(features);
							log("Go upper layer: " + go);
							if (go)
							{
								this.mergeNewParams({
									"layers": [this.realmsVectors[this.layerLevel].layer]
								});
								if (typeof (this.labelLayer) != "undefined" && this.labelLayer != null)
									this.labelLayer.mergeNewParams({
										"layers": [this.realmsVectors[this.layerLevel].label]
									});
								return;
							}
							else
								this.layerLevel++;
						}
					}
					this.lastZoom = this.map.getZoom();
				}
			}

			if (typeof (this.realmsVectors[this.layerLevel]) === "undefined" || this.realmsVectors[this.layerLevel] == null)
				return;
			var features = this.realmsVectors[this.layerLevel].features;
			var count = 0;
			go = this.goNextLevel(features);

			if (go)
			{
				this.mergeNewParams({
					"layers": [this.realmsVectors[this.layerLevel].layer]
				});
				if (typeof (this.labelLayer) != "undefined" && this.labelLayer != null)
					this.labelLayer.mergeNewParams({
						"layers": [this.realmsVectors[this.layerLevel].label]
					});
			}
		}

	},

	onMoveStart: function(evt)
	{
		if (typeof (evt.zoomChanged) != "undefined")
			this.zoomChanged = evt.zoomChanged;
		else
			this.zoomChanged = false;

		log("zoomChanged is now " + this.zoomChanged);

	},

	onLoadEnd: function(evt)
	{

	},

	countVisibleFeature: function(features)
	{
		var count = 0;
		for (var i = 0; i < features.length; i++)
		{
			// log(this.features[i].attributes.name + " is visible? " + this.features[i].onScreen());
			var isInt = false;
			for (var j = 0; j < features[i].geometry.components.length; j++)
			{
				if (features[i].geometry.components[j].bounds.intersectsBounds(this.map.getExtent(), true))
				{
					isInt = true;
					count++;
					break;
					;
				}
			}
			log(features[i].attributes.name + " bounds is visible? " + isInt);
		}
		return count;
	},

	countContainedFeature: function(features)
	{
		var count = 0;
		// Check if viewport contains any features
		for (var i = 0; i < features.length; i++)
		{
			for (var j = 0; j < features[i].geometry.components.length; j++)
			{
				if (this.map.getExtent().containsBounds(features[i].geometry.components[j].bounds))
				{
					isInt = true;
					count++;
				}
			}
		}
		return count;
	},

	goNextLevel: function(features)
	{
		// If the current layer level is already the last level, simply return
		if (this.layerLevel == this.realmsVectors.length - 1)
			return false;
		var count = this.countContainedFeature(features);
		// if viewport does not fully contain a single feature. This accounts for the case where view is majority on a single entity but
		// on the borders of the viewport there are other entities. Go to next level in that case
		if (count == 0)
		{
			for (var i = ++this.layerLevel; i < this.realmsVectors.length; i++)
			{
				var countContain = this.countContainedFeature(this.realmsVectors[i].features);
				var countIntersect = this.countVisibleFeature(this.realmsVectors[i].features);
				// if viewport contains 1, then check for intersection
				if (countContain == 1 || countIntersect >= 1)
				{
					this.layerLevel = i;
					if (countIntersect > 1)
						return true;
				}
			}
		}
		count = this.countVisibleFeature(features);
		// for (var i = 0; i < features.length; i++)
		// {
		// //log(this.features[i].attributes.name + " is visible? " + this.features[i].onScreen());
		// var isInt = false;
		// for (var j = 0; j < features[i].geometry.components.length; j++)
		// {
		// if (features[i].geometry.components[j].bounds.intersectsBounds(this.map.getExtent()))
		// {
		// isInt = true;
		// count++;
		// break;
		// }
		// }
		// log(features[i].attributes.name + " bounds is visible? " + isInt);
		// }

		if (count <= 1)
		{
			for (var i = ++this.layerLevel; i < this.realmsVectors.length; i++)
			{
				// do something here.
				var count = this.countVisibleFeature(this.realmsVectors[i].features);
				if (count >= 1)
				{
					this.layerLevel = i;
					if (count > 1)
						break;
				}
			}
			return true;

		}
		return false;
	},

	goPreviousLevel: function(features)
	{
		var countContain = this.countContainedFeature(features);
		var countIntercept = this.countContainedFeature(features);
//		if (countContain == 0)
//		{
//			for (var i = this.layerLevel; i >= 0; i--)
//			{
//				var countContain = this.countContainedFeature(this.realmsVectors[i].features);
//				var countIntersect = this.countVisibleFeature(this.realmsVectors[i].features);
//				// if viewport contains 1, then check for intersection
//				if (countContain == 1 || countIntersect >= 1)
//				{
//					this.layerLevel = i;
//					if (countIntersect > 1)
//						return true;
//				}
//			}
//		}
		count = this.countVisibleFeature(features);
		if (count >= 1)
		{
			if((this.layerLevel) == 0)
				return true;
			for (var i = --this.layerLevel; i >= 0; i--)
			{
				// do something here.
				var count = this.countVisibleFeature(this.realmsVectors[i].features);
				if (count >= 1)
				{
					this.layerLevel = i;
				}
				else
				{
					return true;
				}
			}
			return true;
		}
		return false;
	},

	CLASS_NAME: "OpenLayers.Layer.WMSMAF"

});